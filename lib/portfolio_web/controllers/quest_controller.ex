defmodule PortfolioWeb.QuestController do
  use PortfolioWeb, :controller

  alias Portfolio.Quests
  alias Portfolio.Quests.Quest

  def index(conn, _params) do
    quests = Quests.list_quests()
    render(conn, :index, quests: quests)
  end

  def new(conn, _params) do
    changeset = Quests.change_quest(%Quest{})
    render(conn, :new, changeset: changeset)
  end

  def create(conn, %{"quest" => quest_params}) do
    case Quests.create_quest(quest_params) do
      {:ok, quest} ->
        conn
        |> put_flash(:info, "Quest created successfully.")
        |> redirect(to: ~p"/quests/#{quest}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    quest = Quests.get_quest!(id)
    render(conn, :show, quest: quest)
  end

  def edit(conn, %{"id" => id}) do
    quest = Quests.get_quest!(id)
    changeset = Quests.change_quest(quest)
    render(conn, :edit, quest: quest, changeset: changeset)
  end

  def update(conn, %{"id" => id, "quest" => quest_params}) do
    quest = Quests.get_quest!(id)

    case Quests.update_quest(quest, quest_params) do
      {:ok, quest} ->
        conn
        |> put_flash(:info, "Quest updated successfully.")
        |> redirect(to: ~p"/quests/#{quest}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, quest: quest, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    quest = Quests.get_quest!(id)
    {:ok, _quest} = Quests.delete_quest(quest)

    conn
    |> put_flash(:info, "Quest deleted successfully.")
    |> redirect(to: ~p"/quests")
  end
end
