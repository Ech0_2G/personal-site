defmodule PortfolioWeb.QuestHTML do
  use PortfolioWeb, :html

  embed_templates "quest_html/*"

  @doc """
  Renders a quest form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def quest_form(assigns)
end
