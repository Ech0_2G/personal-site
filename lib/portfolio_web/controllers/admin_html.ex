defmodule PortfolioWeb.AdminHTML do
  use PortfolioWeb, :html

  embed_templates "admin_html/*"
end
