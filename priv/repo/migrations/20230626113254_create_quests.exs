defmodule Portfolio.Repo.Migrations.CreateQuests do
  use Ecto.Migration

  def change do
    create table(:quests) do
      add :name, :string
      add :description, :text
      add :completed, :boolean, default: false, null: false

      timestamps()
    end
  end
end
