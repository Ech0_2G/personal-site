defmodule Portfolio.QuestsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Portfolio.Quests` context.
  """

  @doc """
  Generate a quest.
  """
  def quest_fixture(attrs \\ %{}) do
    {:ok, quest} =
      attrs
      |> Enum.into(%{
        name: "some name",
        description: "some description",
        completed: true
      })
      |> Portfolio.Quests.create_quest()

    quest
  end
end
