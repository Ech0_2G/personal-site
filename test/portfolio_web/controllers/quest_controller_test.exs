defmodule PortfolioWeb.QuestControllerTest do
  use PortfolioWeb.ConnCase

  import Portfolio.QuestsFixtures

  @create_attrs %{name: "some name", description: "some description", completed: true}
  @update_attrs %{name: "some updated name", description: "some updated description", completed: false}
  @invalid_attrs %{name: nil, description: nil, completed: nil}

  describe "index" do
    test "lists all quests", %{conn: conn} do
      conn = get(conn, ~p"/quests")
      assert html_response(conn, 200) =~ "Listing Quests"
    end
  end

  describe "new quest" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/quests/new")
      assert html_response(conn, 200) =~ "New Quest"
    end
  end

  describe "create quest" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/quests", quest: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/quests/#{id}"

      conn = get(conn, ~p"/quests/#{id}")
      assert html_response(conn, 200) =~ "Quest #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/quests", quest: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Quest"
    end
  end

  describe "edit quest" do
    setup [:create_quest]

    test "renders form for editing chosen quest", %{conn: conn, quest: quest} do
      conn = get(conn, ~p"/quests/#{quest}/edit")
      assert html_response(conn, 200) =~ "Edit Quest"
    end
  end

  describe "update quest" do
    setup [:create_quest]

    test "redirects when data is valid", %{conn: conn, quest: quest} do
      conn = put(conn, ~p"/quests/#{quest}", quest: @update_attrs)
      assert redirected_to(conn) == ~p"/quests/#{quest}"

      conn = get(conn, ~p"/quests/#{quest}")
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, quest: quest} do
      conn = put(conn, ~p"/quests/#{quest}", quest: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Quest"
    end
  end

  describe "delete quest" do
    setup [:create_quest]

    test "deletes chosen quest", %{conn: conn, quest: quest} do
      conn = delete(conn, ~p"/quests/#{quest}")
      assert redirected_to(conn) == ~p"/quests"

      assert_error_sent 404, fn ->
        get(conn, ~p"/quests/#{quest}")
      end
    end
  end

  defp create_quest(_) do
    quest = quest_fixture()
    %{quest: quest}
  end
end
